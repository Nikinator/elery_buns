from __future__ import absolute_import

from celery import shared_task
from celery import Task
from celery_buns import celery_app
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

# Abstract classes are not registered, but are used as the base class for new task types.
class DebugTask(Task):
    abstract = True

    # Handlers
    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        print 'after_return!'

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        print 'failure!!'

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        print 'retry!'

    def on_success(self, retval, task_id, args, kwargs):
        print 'success!!!'

@celery_app.task(base=DebugTask)
def add(x, y):
    logger.info('Adding  {0}  + {1}'.format(x, y))
    assert 0
    return x + y


@celery_app.task(bind=True)
def upload_files(self, filenames):
    for i, file in enumerate(filenames):
        if not self.request.called_directly:
            self.update_state(state='PROGRESS',
                              meta={'current': i, 'total': len(filenames)})
