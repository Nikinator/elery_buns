from django.conf.urls import include, url
from .views import *

urlpatterns = [
    url(r'^$', app_view),
    url(r'^add/', add_view),
]