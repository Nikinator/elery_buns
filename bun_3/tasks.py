from __future__ import absolute_import
from celery import group
from celery.exceptions import MaxRetriesExceededError, Retry

from celery_buns import celery_app
import time

@celery_app.task(bind=True)
def send(self, msg, email, **kw):
    time.sleep(5)  # burden
    # real_send(...)
    result = True

    # if email not in send.emails:
    #     result = False
    #     send.emails.append(email)
    if email == 'cc@c.cc':
        result = False

    if result:
        print 'Msg has been success sent to email "{0}"'.format(email)
        # return result
    else:
        # print 'Could not send a message to email "{0}. Msg will be resend."'.format(email)
        try:
            self.retry(countdown=kw.get('countdown', 0), max_retries=kw.get('max_retries', 0))
            result = True
        except (MaxRetriesExceededError, Retry):
            # print 'Sorry, a message doesn\'t send to "{0}"'.format(email)
            result = False
    return result
send.emails = []


@celery_app.task(bind=True)
def send_message(self, msg, emails):
    """
    msg - str
    emails - list of str
    """
    data_tasks = group(
        send.s(msg, email, max_retries=2, countdown=3) for email in emails).skew(start=0,stop=len(emails))
    tasks_results = data_tasks()
    print 11111, dir(tasks_results)
    send_cnt = 0
    for res in tasks_results.iterate():
        print 1111, res, type(res)
        if res:
            send_cnt+=1
    print 'Msg send to {0} emailes. Errors: {1}'.format(send_cnt, len(emails)-send_cnt)
    return send_cnt
