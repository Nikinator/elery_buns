from django.shortcuts import render
from django.http import HttpResponse
from .models import TaskScheduler
from .tasks import *


def app_view(request):
    hour = request.GET.get('hour', 9)
    minute = request.GET.get('minute', 20)
    map(TaskScheduler.terminate, TaskScheduler.objects.all())
    ts = TaskScheduler.schedule_every(
        'bun_5.tasks.Bun5PeriodicTask', hour, minute)
    ts.start(); print(ts)
    return render(request, 'bun_5.html')