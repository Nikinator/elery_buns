# coding: utf-8

import datetime

from django.db import models
from djcelery.models import PeriodicTask, CrontabSchedule


class TaskScheduler(models.Model):
    periodic_task = models.ForeignKey(PeriodicTask)

    @staticmethod
    def schedule_every(task_name, hour, minute, args='[]', kwargs='{}'):
        """
        schedules a task by name
        """
        ptask_name = "%s_%s" % (task_name, datetime.datetime.now())
        crontab_schedules = CrontabSchedule.objects.filter(
            hour=hour, minute=minute,  day_of_week='*', day_of_month='*', month_of_year='*')
        crontab_schedule = (
            crontab_schedules[0] if crontab_schedules.exists()
            else CrontabSchedule.objects.create(hour=hour, minute=minute))
        ptask = PeriodicTask.objects.create(
            name=ptask_name, task=task_name,
            crontab=crontab_schedule, args=args, kwargs=kwargs,
            # queue=gs_settings.CELERY_WORKER_TASK_QUEUE
        )
        return TaskScheduler.objects.create(periodic_task=ptask)

    def stop(self):
        self.periodic_task.enabled = False
        self.periodic_task.save()

    def start(self):
        self.periodic_task.enabled = True
        self.periodic_task.save()

    def terminate(self):
        self.stop()
        self.delete()
        self.periodic_task.delete()