from __future__ import absolute_import

import time
from celery import Task
from celery_buns import celery_app


class Bun5PeriodicTask(Task):
    def run(self, *args, **kwargs):
        print 'Run bun5'
        time.sleep(5)

    def on_success(self, retval, task_id, args, kwargs):
        print 'succes!!!'