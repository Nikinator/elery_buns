from django.shortcuts import render
from django.http import HttpResponse

def proj_view(request):
	return render(request, 'root_page.html')
