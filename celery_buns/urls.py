"""celery_buns URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from . import views
from bun_1 import urls as bun_1_urls
from bun_2 import urls as bun_2_urls
from bun_3 import urls as bun_3_urls
from bun_4 import urls as bun_4_urls
from bun_5 import urls as bun_5_urls
# from dbmail import urls as dbmail_urls


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.proj_view),
    url(r'^bun_1/', include(bun_1_urls)),
    url(r'^bun_2/', include(bun_2_urls)),
    url(r'^bun_3/', include(bun_3_urls)),
    url(r'^bun_4/', include(bun_4_urls)),
    url(r'^bun_5/', include(bun_5_urls)),
    # url(r'^dbmail/', include(dbmail_urlsdbmail_urls)),
]