from __future__ import absolute_import

from celery_buns import celery_app


@celery_app.task
def add(x, y):
    return x + y

def foo():
    #You can also convert chunks to a group:
    group = add.chunks(zip(range(100), range(100), 10)).group()
    #and with the group skew the countdown of each task by increments of one:
    group.skew(start=1, stop=10)()
    # which means that the first task will have a countdown of 1, the second a countdown of 2 and so on.
    res = group.get()
    return res